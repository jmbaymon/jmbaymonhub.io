//see the document object contents
// console.dir(document);

// console.log(document.domain);
// console.log(document.title);
// console.log(document.URL);
// console.log(document.doctype);
// console.log(document.head);
// console.log(document.all);

// var odd = document.querySelectorAll('li:nth-child(odd)');
// var even = document.querySelectorAll('li:nth-child(even)');
//
// for(var i=0; i< odd.length; i++){
//
// odd[i].style.backgroundColor = 'lightgreen';
// even[i].style.backgroundColor = 'teal';}

//Traversing the dom
var itemList =  document.querySelector('#items');
//parentNode or parentElement
//console.log(itemList.parentNode);
itemList.parentNode.style.backgroundColor = "Silver";


//children

console.log(itemList.children);
console.log(itemList.children[1]);
itemList.children[1].style.backgroundColor ='yellow';
